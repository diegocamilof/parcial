<?php
include "presentacion/menu.php";
require_once "logica/juguete.php";


// require 'logica/Persona.php';
// require 'logica/Paciente.php';

$error=1;
$nombre = "";
$material = "";
$cantidad= "";

if(isset($_POST["insertar"])){
    $nombre = $_POST["nombre"];
    $material = $_POST["material"];
    $cantidad = $_POST["cantidad"];
    $juguete = new juguete("", $nombre, $material, $cantidad);
    $juguete -> insertar();
    $error=0;
}

?>



	<div class="container">
		<div class="row">
			<?php include 'encabezado.php';?>
		</div>
		<div class="row">
			<div class="col-3"></div>
			<div class="col-6">
				<div class="card">
					<div class="card-header bg-info text-white">Insertar Juguete</div>
					<div class="card-body">
						<?php 
						if($error == 0){
						?>
						<div class="alert alert-success" role="alert">
							Juguete insertado exitosamente.
						</div>
						<?php }?>
						<form action=<?php echo "index.php?pid=" . base64_encode("presentacion/insertarjuguete.php")."&nos=true" ?> method="post">
							<div class="form-group">
								<input type="text" name="nombre" class="form-control" placeholder="Nombre" required="required" >
							</div>
							<div class="form-group">
								<input type="text" name="material" class="form-control" placeholder="Material" required="required" >
							</div>
							<div class="form-group">
								<input type="number" name="cantidad" class="form-control" placeholder="Cantidad" required="required" >
							</div>
							
							<button type="submit" name="insertar" class="btn btn-info">Insertar</button>
						</form>
					</div>
				</div>
			</div>

		</div>

	</div>

