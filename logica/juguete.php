<?php
require_once "persistencia/jugueteDAO.php";
require_once "persistencia/Conexion.php";

class juguete{
    private $id;
    private $nombre;
    private $material;
    private $cantidad;
    private $conexion;
    private $jugueteDAO;
    
    
    function getmaterial(){
        return $this -> material;
    }
    function getcantidad(){
        return $this -> cantidad;
    }
    function getnombre(){
        return $this -> nombre;
    }
    
    public function juguete($id="",$nombre="",$material="",$cantidad="") {
        $this -> idAdministrador = $id;
        $this -> nombre = $nombre;
        $this -> material = $material;
        $this -> cantidad = $cantidad;
        $this -> conexion=new Conexion();
        $this -> jugueteDAO=new jugueteDAO($this -> id, $this -> nombre, $this -> material, $this -> cantidad);
    }
    public function consultar($cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> jugueteDAO-> consultarPaginacion($cantidad, $pagina));
        $juguetes = array();
        $this -> conexion -> cerrar();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $j = new juguete($resultado[0], $resultado[1], $resultado[2]);
            array_push($juguetes, $j);
        }
        $this -> conexion -> cerrar();
        return $juguetes;
    }
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> jugueteDAO -> insertar());
        $this -> conexion -> cerrar();
    }
   }

?>